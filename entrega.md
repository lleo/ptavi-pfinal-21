## Parte básica
1. Lanzamos **serversip.py**:  *python3 serversip.py 6001*

![](captura-sip.png)


2. Lanzamos **serverrtp.py**:  *python3 serverrtp.py 127.0.0.1:6001 cancion.mp3*

![](captura-rtp.png)

3. Lanzamos **client.py**: *python3 client.py 127.0.0.1:6001 sip:yo@clientes.net sip:cancion@signasong.net 10 recived.mp3*

client.py se encuentra en la carpeta clientes

![](captura-cliente.png)

## Parte adicional
* Peticiones concurrentes

Lanzando dos clientes distintos:

    -python3 client.py 127.0.0.1:6001 sip:yoa@clientes.net sip:cancion@signasong.net 10 recived2a.mp3
    -python3 client2.py 127.0.0.1:6001 sip:yob@clientes.net sip:cancion@signasong.net 10 recived2b.mp3
* Cabecera de tamaño


En el código lo podemos ver en **client.py** en **invite_method**

![](length.png)

* Gestión de errores 

Se han incluido códigos de error, como el 405
